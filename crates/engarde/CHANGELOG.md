# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

<!-- next-header -->
## [Unreleased] - ReleaseDate

## [0.1.1] - 2022-04-04

<!-- next-url -->
[Unreleased]: https://github.com/cobalt-org/cobalt.rs/compare/engarde-v0.1.1...HEAD
[0.1.1]: https://github.com/cobalt-org/cobalt.rs/compare/b805d1fcd105898446de9431582c38724fe5aa59...engarde-v0.1.1
